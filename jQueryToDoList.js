$(document).ready(function(){

  /**
   * Toggles "done" class on <li> element
   */
     function doneClass (){
       $(this).closest("li").toggleClass("done");
     };

     $("li").on('click', doneClass);

  /**
   * Delete element when delete link clicked
   */
  function deleteElement(){$(".delete").click(function(){
     $((this).closest("li").remove(":contains('delete')"));
   });}
   $(".delete").on('click', deleteElement);
  /**
   * Adds new list item to <ul>
   */
  const addListItem = function() {
    const text = $('input').val();

    // rest here..
    const newSpan=`<span>${text}</span>`;
    const deleteBtn="<a class=\"delete\">Delete</a>";
    const newLi = `<li>${newSpan}${deleteBtn}</li>`;
      $('ul').append(newLi);
      $(newLi).append(deleteBtn);
      $("li").on('click', doneClass);

    deleteElement();
    doneClass();
  };

  // add listener for add
  $(".add-item").on('click', (addListItem));

});
