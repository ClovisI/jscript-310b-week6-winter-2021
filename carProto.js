$(document).ready(function(){
  
// /**
//  * Car class
//  * @constructor
//  * @param {String} model
//  */
class Car {
  constructor(model) {
    this.currentSpeed = 0;
    this.model = model;
  }

    accelerate() {this.currentSpeed++};
    brake() { this.currentSpeed--};
    stringify= () => {return `The ${this.model} is traveling at ${this.currentSpeed} mi/h.`}
  }

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const taurus = new Car('Taurus');
taurus.accelerate();
taurus.accelerate();
taurus.brake();
console.log(taurus.stringify());

// /**
//  * ElectricCar class
//  * @constructor
//  * @param {String} model
//  */

class ElectricCar extends Car{
  constructor(model) {
    super(model);
    this.motor="electric";
  }

  accelerate() {
    super.accelerate(),
    super.accelerate();
  }

  stringify= () => {return `The Electric ${this.model} is traveling at ${this.currentSpeed} mi/h.`}
}

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()

const taycan = new ElectricCar('Taycan');
taycan.accelerate();
taycan.accelerate();
taycan.brake();
console.log(taycan.stringify());

});
