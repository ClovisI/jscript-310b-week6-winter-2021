$(document).ready(function(){
  
// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>
const aCTA="<a href=\"#\"id=\"cta\">Buy Now!</a>";
$("p").after(aCTA);

// Access (read) the data-color attribute of the <img>,
// log to the console
const dataColor =  $("img").attr('data-color');
console.log(dataColor);

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"
const third = $('li').get(2).className="highlight";

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
$('p').remove(0);

// Create a listener on the "Buy Now!" link that responds to a click event.
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"
$('a').click(() =>{
  const cartMsg = $('a').after("Added to cart");
  $('a').remove(0);
});

});
